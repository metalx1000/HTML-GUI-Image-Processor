#!/bin/bash
echo "Content-type: text/plain\n\n"
echo "Form variables :"
echo ""
eval $(echo "$QUERY_STRING"|awk -F'&' '{for(i=1;i<=NF;i++){print $i}}')
#file=`busybox httpd -d $file`
echo "converting $file"
mkdir -p "../old"
cp "../images/$file" "../old/"
convert "../images/$file" -set colorspace Gray "../images/$file"
convert "../thumbs/$file" -set colorspace Gray "../thumbs/$file"
echo ""
echo ""
